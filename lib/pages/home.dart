import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx_demo/store/counter.dart';

class HomePage extends StatelessWidget {
  final String title;
  HomePage({Key? key, required this.title}) : super(key: key);
  final Counter counter = Counter();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Text(
            'Counter',
            style: TextStyle(fontSize: 30),
          ),
          Observer(builder: (_) {
            return Text(
              '${counter.count}',
              style: const TextStyle(fontSize: 30),
            );
          }),
          TextButton(
            onPressed: counter.increment,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Text('Add'),
                Icon(Icons.add),
              ],
            ),
          ),
          TextButton(
              onPressed: counter.decrement,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Text('Minus'),
                  Icon(Icons.remove),
                ],
              ))
        ],
      ),
    );
  }
}
